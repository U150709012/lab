public class FindPrimes {
	public static void main(String[] args){

		int limit = Integer.parseInt(args[0]);

		for(int number=2; number<limit; number++){
			boolean isPrime = true;
			
			
			for (int j=2;j<number;j++)
			{
				if (number % j ==0){
					isPrime = false;
					break;
				}
			
			}
			if(isPrime){
				System.out.print(number+ " ");
			}
		}

	}
}
