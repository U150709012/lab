package mert.shapes3d;

import mert.shapes.Square;

public class Cube extends Square {
	
	public Cube(){
		this(5);
	}
	public Cube(int side){
		super(side);
	}
	public double area(){
		return 6 * super.area();
	}
	public double volume(){
		return super.area() * side;
	}
	public String toString(){
		return " Cube side = " + side;
				
	}

}
