package mert.main;
import java.util.ArrayList;

import mert.shapes3d.*;

public class Test3D {
	public static void main(String[] args){
		
		ArrayList<Cylinder> cylinders = new ArrayList<Cylinder>();
		Cylinder cylinder = new Cylinder();
		
			cylinders.add(cylinder);
			cylinders.add(new Cylinder(6,7));
			System.out.println(cylinders.toString());
			
		ArrayList<Cube> cubes = new ArrayList<Cube>();
		Cube cube = new Cube(6);
			
			cubes.add(cube);
			cubes.add(new Cube(5));
			System.out.println(cubes.toString());
			
	}		

}
